# deploy

#### 项目介绍
maven 支持多台部署，支持根据环境，自动替换文件

#### 软件架构
python3编写，依赖pscp.exe连接linux服务器


#### 安装教程
1. 无需安装，已打包成exe文件，直接使用只需要dist目录下文件（打包命令：pyinstaller -F deploy.py）
2. 部署应用名称.bat（启动文件，复制并重命名文件即可）
3. 部署应用名称.conf(配置信息)
4. 部署应用名称（文件夹，按项目的结构，放置配置文件）
5. 部署应用的配置可以任意目录下，只要在同一文件夹下就行。
6. 不在根目录下需要修改.bat 中deploy.exe的路径信息

#### 使用说明

1. 复制.bat文件并重命名为：部署应用名称
2. 新建：部署应用名称.conf
3. 配置内容说明(web上这里会排版混乱，请看cypt-test.conf)：
    [info]
    #服务器连接用户名称
    username = 
    #服务器连接密码
    pwd=
    #服务器依次对应的tomcat路径
    webappsPaths = /data/apache-tomcat-8.5.11/webapps/
    #服务依次对应的ip地址及其端口
    ips = 10.166.116.160:8080
    #部署完成后打开链接地址相对应用名称路径
    openUrl=webInfo/index
    #环境id(指定生产or测试环境（依赖maven配置替换原则，建议用deploy的替换方式）)
    profileId=pro
    #maven所在bin目录
    mvnBin=D:\"Program Files"\Java\maven3.2.1\bin\
    #项目所在路径
    projectPath=E:\ideaWorkplace\cypt\
    #打包后的应用名称（不一定是war）
    warName = cypt-0.0.1-SNAPSHOT.war
    #是否第一次链接（首次连接询问是否要缓存的解决）
    isFirst = false

4.直接启动bat

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)